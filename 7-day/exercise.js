import {moment} from "moment";

export {moment}
class Dates {
    today : Date;
    constructor() {
        this.today = new Date()
    }


    getToday() {
        return moment(this.today).startOf("date").format("YYYY-MM-DD");
    }

    getTomorrow() {
        return moment(this.today).add(1, "days").format("YYYY-MM-DD");
    }


    getYesterday() {
        return moment(this.today).subtract(1, "days").format("YYYY-MM-DD");
    }

    getLastWeek() {
        return moment(this.today).subtract(7, "days").format("YYYY-MM-DD");
    }

    getNextWeek() {
        return moment(this.today).add(7, "days").format("YYYY-MM-DD");
    }
}

const dates = new Dates();
console.log("GetToday", dates.getToday())
console.log("getTomorrow", dates.getTomorrow());
console.log("getYesterday" ,dates.getYesterday());
console.log("getLastWeek" ,dates.getLastWeek());
console.log("getNextWeek",dates.getNextWeek());