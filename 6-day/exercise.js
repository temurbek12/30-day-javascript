
for(let i=0; i<=5; i ++ ){
    console.log(i)
}

for (let i = 5; i>=0; i--){
    console.log(i)
}


for(let i=0; i <= 5; i++){
    console.log(`${i} * ${i} = ${i * i}`)
}


const countries = ['Finland', 'Sweden', 'Denmark', 'Norway', 'Iceland']
const newArr = []
for (let i=0; i<countries.length; i++){
    newArr.push(countries[i].toUpperCase())
    console.log(newArr)
}


const number = [1,2,3,4,5]
let sum = 0
for (let i = 0; i<number.length; i++){
    sum = sum + number[i]
    console.log(sum)
}


const number = [1,2,3,4,5]
const newArr = []
let sum = 0
for(let i =0; i<number.length; i++){
    newArr.push(number[i] ** 2)
}
console.log(newArr)


const countries = ['finland', 'Sweden', 'Norway', 'Denmark', 'Iceland']
const newArr = []
for (let i = 0; i< countries.length; i++){
    newArr.push(countries[i].toUpperCase())
}
console.log(newArr)



let i = 0
while (i<=5){
    console.log(i)
    i++
}


let i= 0
do {
    console.log(i)
    i++
} while (i<=5)

































// // Iterate 0 to 10 using for loop
// for (let i = 0; i <= 10; i++) {
//     console.log(i);
// }
//
// // Iterate 0 to 10 using while loop
// let i = 0;
// while (i <= 10) {
//     console.log(i);
//     i++;
// }
//
// // Iterate 0 to 10 using do-while loop
// let t = 0;
// do {
//     console.log(t);
//     t++;
// } while (t <= 10);
//
// // Iterate 10 to 0 using for loop
// for (let k = 10; k >= 0; k--) {
//     console.log(k);
// }
//
// // Iterate 10 to 0 using while loop
// let k = 10;
// while (k >= 0) {
//     console.log(k);
//     k--;
// }
//
// // Iterate 10 to 0 using do-while loop
// let l = 10;
// do {
//     console.log(l);
//     l--;
// } while (l >= 0);
//
// // Iterate 0 to n using for loop
// let n = 5;
// for (let m = 0; m <= n; m++) {
//     console.log(m);
// }
//
// // Print a pattern using loop
// for (let row = 1; row <= 7; row++) {
//     let pattern = '';
//     for (let col = 1; col <= row; col++) {
//         pattern += '#';
//     }
//     console.log(pattern);
// }
//
// // Print the multiplication table using loop
// for (let num = 0; num <= 10; num++) {
//     console.log(`${num} x ${num} = ${num * num}`);
// }
//
// // Print the pattern using loop
// for (let num = 0; num <= 10; num++) {
//     console.log(`${num}   ${num * num}    ${num * num * num}`);
// }
//
// // Print even numbers using loop
// for (let num = 0; num <= 100; num++) {
//     if (num % 2 === 0) {
//         console.log(num);
//     }
// }
//
// // Print odd numbers using loop
// for (let num = 0; num <= 100; num++) {
//     if (num % 2 !== 0) {
//         console.log(num);
//     }
// }
//
// // Check if a number is prime
// function isPrime(number) {
//     if (number <= 1) {
//         return false;
//     }
//     for (let i = 2; i <= Math.sqrt(number); i++) {
//         if (number % i === 0) {
//             return false;
//         }
//     }
//     return true;
// }
//
// // Print prime numbers using loop
// for (let num = 0; num <= 100; num++) {
//     if (isPrime(num)) {
//         console.log(num);
//     }
// }
//
// // Calculate the sum of all numbers using loop
// let sum = 0;
// for (let num = 0; num <= 100; num++) {
//     sum += num;
// }
// console.log('The sum of all numbers from 0 to 100 is', sum);
//
// // Calculate the sum of evens and odds using loop
// let sumEvens = 0;
// let sumOdds = 0;
// for (let num = 0; num <= 100; num++) {
//     if (num % 2 === 0) {
//         sumEvens += num;
//     } else {
//         sumOdds += num;
//     }
// }
// console.log('The sum of all evens from 0 to 100 is', sumEvens);
// console.log('The sum of all odds from 0 to 100 is', sumOdds);
//
// // Store the sum of evens and odds in an array
// let sumsArray = [sumEvens, sumOdds];
// console.log(sumsArray);
//
// // Generate an array of 5 random numbers
// let randomNumbers = [];
// for (let i = 0; i < 5; i++) {
//     let randomNumber = Math.floor(Math.random() * 100);
//     randomNumbers.push(randomNumber);
// }
// console.log(randomNumbers);
//
// // Generate an array of 5 unique random numbers
// let uniqueRandomNumbers = [];
// while (uniqueRandomNumbers.length < 5) {
//     let randomNumber = Math.floor(Math.random() * 100);
//     if (!uniqueRandomNumbers.includes(randomNumber)) {
//         uniqueRandomNumbers.push(randomNumber);
//     }
// }
// console.log(uniqueRandomNumbers);
//
// // Generate a six-character random ID
// let characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
// let randomId = '';
// for (let i = 0; i < 6; i++) {
//     let randomIndex = Math.floor(Math.random() * characters.length);
//     randomId += characters[randomIndex];
// }
// console.log(randomId);