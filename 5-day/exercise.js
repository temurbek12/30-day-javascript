
// Declare an array with more than 5 elements
let numbers = [1, 2, 3, 4, 5, 6];

// Find the length of the array
// console.log(numbers.length);

// Get the first item, middle item, and last item of the array
let firstItem = numbers[0];
let middleItem = numbers[Math.floor(numbers.length / 2)];
let lastItem = numbers[numbers.length - 1];
// console.log('First item:', firstItem);
// console.log('Middle item:', middleItem);
// console.log('Last item:', lastItem);

// Declare an array with mixed data types and find its length
let mixedDataTypes = ['Bobur', 25, true, null, {name: 'Anora'}];
// console.log('Length of mixedDataTypes:', mixedDataTypes.length);

// Declare an array of IT companies
let itCompanies = ['Facebook', 'Google', 'Microsoft', 'Apple', 'IBM', 'Oracle', 'Amazon'];

// Print the array
// console.log(itCompanies);

// Print the number of companies in the array
// console.log('Number of companies:', itCompanies.length);

// Print the first, middle, and last companies
// console.log('First company:', itCompanies[0]);
// console.log('Middle company:', itCompanies[Math.floor(itCompanies.length / 2)]);
// console.log('Last company:', itCompanies[itCompanies.length - 1]);

// Print out each company
itCompanies.forEach(function (company) {
    // console.log(company);
});

// Change each company name to uppercase and print them out
// itCompanies.forEach(function (company, index, array) {
//     array[index] = company.toUpperCase();
//     // console.log(company);
// });

// Print the array as a sentence
let sentence = itCompanies.join(', ') + ' are big IT companies.';
// console.log(sentence);

// Check if a certain company exists in the itCompanies array
let searchCompany = 'Microsoft';
// if (itCompanies.includes(searchCompany)) {
//     console.log(searchCompany + ' exists in the itCompanies array.');
// } else {
//     console.log('Company not found.');
// }


const message = itCompanies.includes(searchCompany) ? searchCompany + ' exists in the itCompanies array.' : searchCompany + ' Company not found.'
console.log(message)



// Filter out companies which have more than one 'o'
let filteredCompanies = itCompanies.filter(function (company) {
    let count = 0;
    for (let i = 0; i < company.length; i++) {
        if (company[i] === 'o' || company[i] === 'O') {
            count++;
        }
    }
    return count > 1;
});



console.log('Filtered companies:', filteredCompanies);

// Sort the array in alphabetical order
itCompanies.sort();
console.log('Sorted array:', itCompanies);

// Reverse the array
itCompanies.reverse();
console.log('Reversed array:', itCompanies);

// Slice out the first 3 companies from the array
let firstThreeCompanies = itCompanies.slice(0, 3);
// console.log('First three companies:', firstThreeCompanies);

// Slice out the last 3 companies from the array
let lastThreeCompanies = itCompanies.slice(-3);
// console.log('Last three companies:', lastThreeCompanies);

// Slice out the middle IT company or companies from the array
let middleIndex = Math.floor(itCompanies.length / 2);
let middleCompanies = [];
if (itCompanies.length % 2 === 0) {
    middleCompanies = itCompanies.slice(middleIndex - 1, middleIndex + 1);
} else {
    middleCompanies = itCompanies.slice(middleIndex, middleIndex + 1);
}
// console.log('Middle companies:', middleCompanies);

// Remove the first IT company from the array
itCompanies.shift();
console.log('Array after removing the first company:', itCompanies);

// Remove the middle IT company or companies from the array
if (itCompanies.length % 2 === 0) {
    itCompanies.splice(middleIndex - 1, 2);
} else {
    itCompanies.splice(middleIndex, 1);
}
// console.log('Array after removing the middle companies:', itCompanies);

// Remove the last IT company from the array
itCompanies.pop();
console.log('Array after removing the last company:', itCompanies);

// Remove all IT companies
itCompanies.length = 0;
console.log('Array after removing all companies:', itCompanies);