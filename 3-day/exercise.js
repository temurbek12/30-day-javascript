// Declare variables and assign values
let firstName = 'Temur';
let lastName = 'Eshmurodov';
let country = 'UZB';
let city = 'Qashqadaryo';
let age = 22;
let isMarried = false;
let year = 2023;

// Use typeof operator to check different data types
console.log(typeof firstName);
console.log(typeof lastName);
console.log(typeof country);
console.log(typeof city);
console.log(typeof age);
console.log(typeof isMarried);
console.log(typeof year);

// Check if type of '10' is equal to 10
console.log(typeof '10' === typeof 10);



// Truthy values
console.log(Boolean(1));
console.log(Boolean('Hello'));
console.log(Boolean({}));

// Falsy values
console.log(Boolean(0));
console.log(Boolean(''));
console.log(Boolean(null));

// Comparison expressions without using console.log()
console.log(4 > 3);
console.log(4 >= 3);
console.log(4 < 3);
console.log(4 <= 3);
console.log(4 == 4);
console.log(4 === 4);
console.log(4 != 4);
console.log(4 !== 4);
console.log(4 != '4');
console.log(4 == '4');
console.log(4 === '4');

// Falsy comparison statement
console.log('python'.length < 'jargon'.length);

// Comparison expressions with console.log()
console.log(4 > 3 && 10 < 12);
console.log(4 > 3 && 10 > 12);
console.log(4 > 3 || 10 < 12);
console.log(4 > 3 || 10 > 12);
console.log(!(4 > 3));
console.log(!(4 < 3));
console.log(!(false));
console.log(!(4 > 3 && 10 < 12));
console.log(!(4 > 3 && 10 > 12));
console.log(!(4 === '4'));

// Check if 'on' is present in 'dragon' and 'python'
console.log(!('dragon'.includes('on') || 'python'.includes('on')));

// Use the Date object to perform different activities
let today = new Date();
console.log(today.getFullYear()); // Get the current year
console.log(today.getMonth() + 1); // Get the current month as a number (January is 0, so +1)
console.log(today.getDate()); // Get the current date
console.log(today.getDay()); // Get the current day as a number (Sunday is 0, Monday is 1, etc.)
console.log(today.getHours()); // Get the current hour
console.log(today.getMinutes()); // Get the current minute
console.log(Math.floor(Date.now() / 1000)); // Get the number of seconds elapsed from January 1, 1970 to now


// Calculate the slope, x-intercept, and y-intercept of a linear equation y = 2x - 2
let x1 = 2;
let y1 = 2;
let x2 = 6;
let y2 = 10;
let slope = (y2 - y1) / (x2 - x1);
let xIntercept = -y1 / slope;
let yIntercept = -x1 * slope + y1;
console.log('Slope:', slope);
console.log('x-intercept:', xIntercept);
console.log('y-intercept:', yIntercept);

// Calculate the slope between two points: (2, 2) and (6, 10)
let x1Point = 2;
let y1Point = 2;
let x2Point = 6;
let y2Point = 10;
let slopePoint = (y2Point - y1Point) / (x2Point - x1Point);
console.log('Slope between the points:', slopePoint);

// Compare the slopes
console.log(slope === slopePoint);


// Compare the lengths of first name and last name
let myFirstName = 'Asabeneh';
let myLastName = 'Yetayeh';
if (myFirstName.length > myLastName.length) {
    console.log('Your first name, ' + myFirstName + ', is longer than your last name, ' + myLastName);
} else {
    console.log('Your last name, ' + myLastName + ', is longer than your first name, ' + myFirstName);
}

// Declare and assign values to myAge and yourAge variables
let myAge = 250;
let yourAge = 25;
let ageDifference = myAge - yourAge;
console.log('I am ' + ageDifference + ' years older than you.');

// Get the user's birth year and determine if they are old enough to drive
let currentYear = today.getFullYear();
let drivingAge = 18;
let ageToDrive = currentYear
if (ageToDrive >= drivingAge) {
    console.log('You are ' + ageToDrive + '. You are old enough to drive.');
} else {
    let yearsToWait = drivingAge - ageToDrive;
    console.log('You are ' + ageToDrive + '. You will be allowed to drive after ' + yearsToWait + ' years.');
}

