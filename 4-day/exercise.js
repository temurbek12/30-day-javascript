let score = 85;
let grade;

if (score >= 80 && score <= 100) {
    grade = 'A';
} else if (score >= 70 && score <= 89) {
    grade = 'B';
} else if (score >= 60 && score <= 69) {
    grade = 'C';
} else if (score >= 50 && score <= 59) {
    grade = 'D';
} else if (score >= 0 && score <= 49) {
    grade = 'F';
} else {
    grade = 'Invalid score';
}

console.log(`The grade for the score ${score} is ${grade}.`);

let month = 'October';
let season;

switch (month.toLowerCase()) {
    case 'september':
    case 'october':
    case 'november':
        season = 'Autumn';
        break;
    case 'december':
    case 'january':
    case 'february':
        season = 'Winter';
        break;
    case 'march':
    case 'april':
    case 'may':
        season = 'Spring';
        break;
    case 'june':
    case 'july':
    case 'august':
        season = 'Summer';
        break;
    default:
        season = 'Invalid month';
}

console.log(`The season for the month ${month} is ${season}.`);


let day = 'Saturday';
let dayType;

switch (day.toLowerCase()) {
    case 'saturday':
    case 'sunday':
        dayType = 'weekend';
        break;
    case 'monday':
    case 'tuesday':
    case 'wednesday':
    case 'thursday':
    case 'friday':
        dayType = 'working day';
        break;
    default:
        dayType = 'Invalid day';
}

console.log("***", `${day} is a ${dayType}.`);