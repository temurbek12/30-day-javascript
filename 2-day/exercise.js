// Declare a variable named challenge and assign it an initial value
let challenge = '30 Days Of JavaScript';

// Print the string on the browser console
console.log(challenge);

// Print the length of the string
console.log(challenge.length);

// Change all the string characters to capital letters
console.log(challenge.toUpperCase());

// Change all the string characters to lowercase letters
console.log(challenge.toLowerCase());

// Cut out the first word of the string
console.log(challenge.substr(challenge.indexOf(' ')));

// Slice out the phrase 'Days Of JavaScript'
console.log(challenge.slice(challenge.indexOf('Days'), challenge.length));

// Check if the string contains the word 'Script'
console.log(challenge.includes('Script'));

// Split the string into an array
console.log(challenge.split(''));

// Split the string '30 Days Of JavaScript' at the space
console.log(challenge.split(' '));

// Split the string 'Facebook, Google, Microsoft, Apple, IBM, Oracle, Amazon' at the comma and change it to an array
let companies = 'Facebook, Google, Microsoft, Apple, IBM, Oracle, Amazon';
console.log(companies.split(', '));

// Change '30 Days Of JavaScript' to '30 Days Of Python'
console.log(challenge.replace('JavaScript', 'Python'));

// Get the character at index 15
console.log(challenge.charAt(15));

// Get the character code of 'J'
console.log(challenge.charCodeAt(challenge.indexOf('J')));

// Use indexOf to determine the position of the first occurrence of 'a' in '30 Days Of JavaScript'
console.log(challenge.indexOf('a'));

// Use lastIndexOf to determine the position of the last occurrence of 'a' in '30 Days Of JavaScript'
console.log(challenge.lastIndexOf('a'));

// Use indexOf to find the position of the first occurrence of the word 'because'
let sentence = 'You cannot end a sentence with because because because is a conjunction';
console.log(sentence.indexOf('because'));

// Use lastIndexOf to find the position of the last occurrence of the word 'because'
console.log(sentence.lastIndexOf('because'));

// Use search to find the position of the first occurrence of the word 'because'
console.log(sentence.search('because'));

// Use trim() to remove any trailing whitespace at the beginning and the end of a string
let whitespaceString = ' 30 Days Of JavaScript ';
console.log(whitespaceString.trim());

// Use startsWith() method with the string '30 Days Of JavaScript' and make the result true
console.log(challenge.startsWith('30 Days'));

// Use endsWith() method with the string '30 Days Of JavaScript' and make the result true
console.log(challenge.endsWith('JavaScript'));

// Use match() method to find all the 'a's in '30 Days Of JavaScript'
console.log(challenge.match(/a/g));

// Use concat() and merge '30 Days of' and 'JavaScript' to a single string, '30 Days Of JavaScript'
let mergedString = '30 Days of '.concat('JavaScript');
console.log(mergedString);

// Use repeat() method to print '30 Days Of JavaScript' 2 times
console.log(challenge.repeat(2));