// let word = 'JavaScript'
//
// word[0] = 'Y'
//
// console.log(word[0])


//Primitive

//NUMBER
// let numOne = 3
// let numTwo = 3
// console.log(numOne == numTwo)


//STRING
// let js = 'JavaScripy'
// let py = 'Python'
// console.log(js == py)


// BOOLEAN
// let lightOn = true
// let lightOff = false
// console.log(lightOff == lightOn)

//NON-PRIMITIVE

// let nums = [1,2,3]
// nums[0] = 10
// console.log(nums)

// let nums = [1,2,3]
// let numbers = [1,2,3]
// console.log(nums == numbers)

// let userOne = {
//     name: 'Asabeneh',
//     role: 'teaching',
//     country: 'Finland'
// }
// let userTwo = {
//     name: 'Asabeneh',
//     role: 'teaching',
//     country: 'Finland'
// }
// console.log(userOne == userTwo)

// let nums = [1,2,3]
// let numbers = nums
// console.log(nums == numbers)

//NUMBERS

// let age = 35
// const gravity = 9.81
// let mass = 72
// const PI = 3.14

//More Examples
// const boilingPoint = 100
// const bodyTemp = 37
// console.log(age,gravity,mass,PI,bodyTemp,boilingPoint)

//Math Object
// const PI =Math.PI
// console.log(PI)

// console.log(Math.round(PI))
// console.log(Math.round(9.81))
// console.log(Math.floor(PI))
// console.log(Math.ceil(PI))
// console.log(Math.min(-5,3,20,4,5,10))
// console.log(Math.max(-5,3,20,4,5,10))

// const rendNum = Math.random()
// console.log(rendNum)

// let randomNum = Math.random()         // generates 0 to 0.999
// let numBtnZeroAndTen = randomNum * 11
//
// // console.log(numBtnZeroAndTen)
//
// let randomNumRoundToFloor = Math.floor(numBtnZeroAndTen)
// console.log(randomNumRoundToFloor)


// let space = ' '
// let firstName = 'Asabeneh'
// let lastName = 'Yetayeh'
// let country = 'Finland'
// let city = 'Helsinki'
// let language = 'JavaScript'
// let job = 'teacher'
// let quote = "The saying,'Seeing is Believing' is not correct in 2020."
// let quotWithBackTick = `The saying,'Seeing is Believing' is not correct in 2020.`
// let fullName = firstName + space + lastName; // concatenation, merging two string together.
// console.log(fullName);